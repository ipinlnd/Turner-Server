package turner;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.FileInputStream;
import java.io.DataInputStream;

@RestController
public class GreetingController {

	Exception ex;

    @GetMapping("/image")
    public @ResponseBody byte[] getImage(@RequestParam(value="id", defaultValue = "0") String id) throws Exception {
    	if (id.equals("0"))
    		return null;
    	Service index = null;
    	for (int i=0;i<Application.categories.size();i++)
    		for (int j = 0;j<Application.categories.get(i).getServices().size(); j++)
    			if (id.equals(Application.categories.get(i).getServices().get(j).getId()))
    				index = Application.categories.get(i).getServices().get(j);
    	
		DataInputStream dis = new DataInputStream(new FileInputStream(new File(index.getImageAddr())));
		byte[] newData = new byte[dis.available()];
        dis.read(newData);
        dis.close();
		return newData;
    }

    @RequestMapping("/category")
    public Category[]	category(@RequestParam(value="id", defaultValue="0") String id)
    {
    	Category[]	categories;
    	if (id.equals("0"))
    	{
    		categories = new Category[Application.categories.size()];
    		for (int i =0;i <Application.categories.size(); i++)
    			categories[i] = Application.categories.get(i);
    		return categories;
    	}

    	categories = new Category[1];
    	for (int i =0;i <Application.categories.size(); i++)
    		if (Application.categories.get(i).getId().equals(id))
    		{
    			categories[i] = Application.categories.get(i);
    			return categories;
    		}
    	return null;
    }
}
