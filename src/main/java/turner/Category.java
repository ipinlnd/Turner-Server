package turner;

import java.util.*;
import java.util.UUID;

public class Category {

    private String name;
    private List<Service>   services;
    private UUID    id;

    public Category(String name) {
        this.name = name;
        id = UUID.randomUUID();
        services = new ArrayList<Service>();
    }

    public String getName() {
        return name;
    }

    public String getId()
    {
        return id.toString().replace("-", "");
    }

    public List<Service>    getServices()
    {
        return services;
    }

    public void addToServices(Service service)
    {
        this.services.add(service);
    }
}
