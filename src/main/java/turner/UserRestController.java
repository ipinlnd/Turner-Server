package turner;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserRestController
{

	@GetMapping
	List<User> getUsers()
	{
		return Application.users;
	}
	
	@GetMapping("/{userId}")
	User getUser(@PathVariable String userId)
	{
    	for (int i=0;i<Application.users.size(); i++)
    	{
    		if (Application.users.get(i).getId().equals(userId))
			{
				
				return Application.users.get(i);
			}
    	}
    	return null;
	}
	
	@GetMapping("/{userId}/{fieldName}")
	String getField(@PathVariable String userId, @PathVariable String fieldName)
	{
		for (int i=0;i<Application.users.size(); i++)
    	{
    		if (Application.users.get(i).getId().equals(userId))
			{
				User user = Application.users.get(i);
				if (fieldName.equals("name"))
					return user.getName();
				else if (fieldName.equals("phoneNumber"))
					return user.getPhoneNumber();
				else if (fieldName.equals("money"))
					return String.valueOf(user.getMoney());
			}
    	}
		return null;
	}
	
	@GetMapping("/phone/{phoneNumber}")
	String getId(@PathVariable String phoneNumber)
	{
		for (int i=0;i<Application.users.size(); i++)
    	{
    		if (Application.users.get(i).getPhoneNumber().equals(phoneNumber))
			{
				return Application.users.get(i).getId();
			}
    	}
		return null;
	}
	
	@RequestMapping("/add")
	int addUser(@RequestParam(value="phone", defaultValue="0") String phoneNumber)
	{
		boolean haveUser = false;
		User newUser = null;
		
		if (phoneNumber.equals("0"))
			return 0;
		
		
		for (int i=0;i<Application.users.size(); i++)
			if (Application.users.get(i).getPhoneNumber().equals(phoneNumber))
			{
				haveUser = true;
				newUser = Application.users.get(i);
			}
		
		if (!haveUser)
		{
			newUser = new User("");
			newUser.setPhoneNumber(phoneNumber);
			Application.users.add(newUser);
		}
		
		try
		{
			Application.writeFile();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return generateRandom();
	}
	
	@GetMapping("/modify/{userId}/{fieldName}/{fieldValue}")
	int modifyUser(@PathVariable String userId, @PathVariable String fieldName, @PathVariable String fieldValue)
	{
		User user = null;
		for (int i=0;i<Application.users.size(); i++)
    	{
    		if (Application.users.get(i).getId().equals(userId))
			{
    			user = Application.users.get(i);
    			break;
			}
    	}
		if (user == null)
			return 0;
		
		if (fieldName.equals("name"))
			user.setName(fieldValue);
		else if (fieldName.equals("money"))
			user.setMoney(Long.getLong(fieldValue));
		else if (fieldName.equals("phoneNumber"))
			user.setPhoneNumber(fieldValue);
		
		try
		{
			Application.writeFile();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return 1;
	}
	
	@GetMapping("/validate/{userId}")
	boolean validateUser(@PathVariable String userId)
	{
		for (int i =0; i< Application.users.size();i++)
			if (Application.users.get(i).getId().equals(userId))
				return true;
		return false;
	}
	
	public int generateRandom()
    {
        Random r = new Random();
        int ans = r.nextInt(9999 - 1000 + 1) + 1000;
        return ans;
    }
	
}
