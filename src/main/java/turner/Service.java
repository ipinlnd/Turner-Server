package turner;

import java.util.*;

public class Service
{
	private String		name;
	private String		id;
	private String		address;
	private List<Line>	lines;
	private String		imageAddr;
	
	
	public String getImageAddr()
	{
		return imageAddr;
	}

	public void setImageAddr(String imageAddr)
	{
		this.imageAddr = imageAddr;
	}

	Service(String name)
	{
		this.name = name;
		lines = new ArrayList<Line>();
		id = UUID.randomUUID().toString().replaceAll("-", "");
		setImageAddr("Data/" + this.id + ".png");
	}
	
	Service(String name, String id)
	{
		this.name = name;
		this.id = id.replaceAll("-", "");
		this.lines = new ArrayList<>();
		setImageAddr("Data/" + this.id + ".png");
	}

	public String getId()
	{
		return id.replaceAll("-", "");
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getName()
	{
		return name;
	}

	public String getAddress()
	{
		return address;
	}

	public void addToLines(String name, String price)
	{
		lines.add(new Line(name, price));
	}

	public List<Line>	getLines()
	{
		return lines;
	}


}