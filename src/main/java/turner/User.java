package turner;

import java.util.UUID;

public class User
{
	private String name;
	private String id;
	private String phoneNumber;
	private long money;

	public User(String name)
	{
		this.name = name;
		id = UUID.randomUUID().toString().replaceAll("-", "");
		money = 0;
	}
	
	public User(String name, String id)
	{
		this.id = id;
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public String getId()
	{
		return id;
	}

	public long getMoney()
	{
		return money;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	public void setMoney(long money)
	{
		this.money = money;
	}
	
	
}