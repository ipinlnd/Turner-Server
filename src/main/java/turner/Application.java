package turner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.*;
import org.json.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

@SpringBootApplication
public class Application 
{

	public static List<User>	users;
    public static List<Category>    categories;

    public static void main(String[] args) 
    {
        users = new ArrayList<>();
        categories = new ArrayList<>();
        loadConfig();

        SpringApplication.run(Application.class, args);
    }

    public static void loadConfig()
    {
        String jsonFile;
		try
		{
			jsonFile = new String(Files.readAllBytes(Paths.get("src/main/java/turner/newconfig.json")), "UTF-8");
			JSONObject obj = new JSONObject(jsonFile);
			
			JSONArray	category = obj.getJSONArray("category");
			for (int i =0;i <category.length(); i++)
			{
				JSONObject categoryObject = category.getJSONObject(i);
				Category newCat = new Category(categoryObject.getString("name"));
				JSONArray services = categoryObject.getJSONArray("services");
				for (int j=0;j < services.length(); j++)
				{
					JSONObject serviceObject = services.getJSONObject(j);
					Service newService = new Service(serviceObject.getString("name"),
													 serviceObject.getString("id"));
					newService.setAddress(serviceObject.getString("address"));
					JSONArray lines = serviceObject.getJSONArray("lines");
					for (int k =0; k< lines.length(); k++)
					{
						JSONObject lineObject = lines.getJSONObject(k);
						newService.addToLines(lineObject.getString("name"), 
								lineObject.getString("price"));
					}
					newCat.addToServices(newService);
				}
				categories.add(newCat);
			}
			
			JSONArray	userArr = obj.getJSONArray("users");
			for (int i =0; i<userArr.length(); i++)
			{
				JSONObject userObj = (JSONObject) userArr.get(i);
				User newUser = new User(userObj.getString("name"),
										userObj.getString("id"));
				newUser.setMoney(userObj.getLong("money"));
				newUser.setPhoneNumber(userObj.getString("phoneNumber"));
				users.add(newUser);
			}
			
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
    }
    
    public static void writeFile() throws IOException
    {
    	OutputStreamWriter newConf = new OutputStreamWriter(new FileOutputStream((new File("src/main/java/turner/newconfig.json"))), "UTF-8");
    	JSONObject obj = new JSONObject();
    	JSONArray userObj = new JSONArray();
    	for (int i =0; i < users.size(); i ++)
    	{
    		JSONObject user = new JSONObject();
    		user.put("id", users.get(i).getId());
    		user.put("name", users.get(i).getName());
    		user.put("money", users.get(i).getMoney());
    		user.put("phoneNumber", users.get(i).getPhoneNumber());
    		userObj.put(user);
    	}
    	
    	JSONArray catObj = new JSONArray();
    	for (int i =0; i < categories.size(); i ++)
    	{
    		JSONObject cat = new JSONObject();
    		cat.put("name", categories.get(i).getName());
    		JSONArray catServObjs = new JSONArray();
    		for (int j =0; j<categories.get(i).getServices().size(); j++)
    		{
    			JSONObject catServ = new JSONObject();
    			Service serv = categories.get(i).getServices().get(j);
    			catServ.put("name", serv.getName());
    			catServ.put("address", serv.getAddress());
    			catServ.put("id", serv.getId());
    			JSONArray servLine = new JSONArray();
    			for(int k = 0; k < serv.getLines().size(); k++)
    			{
    				JSONObject servLineObj = new JSONObject();
    				servLineObj.put("name", serv.getLines().get(k).name);
    				servLineObj.put("price", serv.getLines().get(k).price);
    				servLine.put(servLineObj);
    			}
    			catServ.put("lines", servLine);
    			catServObjs.put(catServ);
    		}
    		cat.put("services", catServObjs);
    		catObj.put(cat);
    	}
    	
    	obj.put("users", userObj);
    	obj.put("category", catObj);
    	newConf.write(obj.toString());
    	
    	newConf.close();
    }

}









